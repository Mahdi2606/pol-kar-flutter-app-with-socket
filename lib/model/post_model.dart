

class PostModel {

  var subject;
  var text;
  var like;
  var commentCount;
  var username;
  var profile;
  List photos;
  var pin;
  var commentMode;
  var likeMode;
  var hashtag;
  var view;

  PostModel.fromJson(Map<String , dynamic> item){
    subject = item['subject'];
    text = item['text'];
    like = item['like'];
    username = item['user']['username'];
    profile = item['user']['profile'];
    commentCount = item['commentCount'];
    photos = item['photos'];
    pin = item['pin'];
    commentMode = item['commentMode'];
    likeMode = item['likeMode'];
    hashtag = item['String'];
    view = item['view'];
  }

}