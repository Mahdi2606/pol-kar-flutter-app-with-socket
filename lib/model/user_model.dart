

class UserModel {

  var name;
  var username;
  var followMode;
  var profile;
  var views;
  var likes;
  var point;
  var settings;
  var family;
  var email;
  var phone;

  UserModel.fromJson(Map<String , dynamic> item){
    print(item);
    name = item['name']; /////
    username = item['username']; /////
    profile = item['profile']; /////
    views = item['views']; /////
    likes = item['likes']; /////
    point = item['point']; /////
    family = item['family']; /////
    email = item['email']; /////
    phone = item['phone']; /////
    settings = item['settings']; /////
    if(item['follow'] != null){
      if(item['follow']['status']){
        followMode = 1;
        print(1);
      }else if(item['follow']['request'] && !item['follow']['status']){
        followMode = 2;
        print(2);
      }else if(!item['follow']['request']){
        followMode = 0;
        print(0);
      }
    }
  }

}