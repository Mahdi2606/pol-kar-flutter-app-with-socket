import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_box.dart';
import 'package:socket_test/model/post_model.dart';

class PostWidget {

 static Widget postList(_posts , _onRefresh){
    return new RefreshIndicator(
        child: new Container(
          color: Colors.white,
          child: new Center(
            child:  _posts.length == 0
                ? new Center(child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(Icons.filter_none,size: 25,color: Colors.grey,) ,
                new SizedBox(width: 10,) ,
                new Text('No post', style: new TextStyle(color: Colors.grey),)
              ],
            ),)
                : new ListView.builder(
              addAutomaticKeepAlives: true,
              padding: EdgeInsets.only(top: 0),
              shrinkWrap: true,
              itemCount: _posts.length,
              itemBuilder: (context , index){
                return PostBox(post: _posts[index],);
              },
            ),
          ),
        ),
        onRefresh: _onRefresh
    );
  }

 static Widget gridPost(_posts , _onRefresh){
    return new RefreshIndicator(
        child: new Container(
          color: Colors.white,
          child: _posts.length == 0
              ? new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(Icons.filter_none,size: 25,color: Colors.grey,) ,
              new SizedBox(width: 10,) ,
              new Text('No post', style: new TextStyle(color: Colors.grey),)
            ],
          )
              : new GridView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemCount: _posts.length,
            itemBuilder: (context , index){
              return new Padding(
                padding: EdgeInsets.all(2),
                child: new Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: _posts[index].photos.length <= 0 ? new AssetImage('assets/images/no_image.png',) : new NetworkImage(
                          'http://192.168.43.3:2000/files/${_posts[index].photos[0]}',
                        ),
                        fit: BoxFit.cover,
                      ),
                      boxShadow: <BoxShadow>[BoxShadow(color: Colors.grey , offset: Offset(0, 0) , blurRadius: 1)],
                    ),
                    child: new Stack(
                      children: <Widget>[
                        new Opacity(
                          opacity: .6,
                          child: new Container(
                            color: Colors.black,
                          ),
                        ),
                        new Align(
                          alignment: Alignment.center,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Icon(Ionicons.getIconData('ios-heart') , color: Colors.white,),
                                  new SizedBox(height: 5,),
                                  new Text('${_posts[index].like}' , style: new TextStyle(color: Colors.white),),
                                ],
                              ),
                              new SizedBox(width: 10,),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Icon(Ionicons.getIconData('ios-eye') , color: Colors.white,),
                                  new SizedBox(height: 5,),
                                  new Text('${_posts[index].view}' , style: new TextStyle(color: Colors.white),),
                                ],
                              ),
                              new SizedBox(width: 10,),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Icon(Ionicons.getIconData('ios-chatbubbles') , color: Colors.white,),
                                  new SizedBox(height: 5,),
                                  new Text('${_posts[index].commentCount}' , style: new TextStyle(color: Colors.white),),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    )
                ),
              );
            },
          )
        ),
        onRefresh: _onRefresh
    );
  }

}