import 'package:flutter/material.dart';


class CustomTextInput extends StatefulWidget{
  final bool obscure;
  final IconData icon;
  final String hint;
  final validator;
  final String helperText;
  final onSave;
//  final Color errorColor;
  CustomTextInput({this.helperText,this.validator,this.hint,this.icon,this.obscure,this.onSave});
  @override
  State<StatefulWidget> createState() => CustomTextInputState();
}

class CustomTextInputState extends State<CustomTextInput>{



  @override
  Widget build(BuildContext context) {
    return  new Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      decoration: new BoxDecoration(

//          gradient: new LinearGradient(
//              colors: <Color>[
//                new Color(0x50FF0000),
//                new Color(0x500000FF)
//              ],
//              begin: Alignment.topLeft,
//              end: Alignment.bottomRight
//          )
      ),
      margin: EdgeInsets.only(top: 20),
      child: new TextFormField(
        onSaved: widget.onSave,
        validator: widget.validator,
        obscureText: widget.obscure,
        style: new TextStyle(color: Colors.white),
        decoration: new InputDecoration(
            hintStyle: new TextStyle(color: Colors.white),
            hintText: widget.hint,

            enabledBorder: UnderlineInputBorder(borderSide: new BorderSide(
              color: Colors.white
            )),
            errorBorder: UnderlineInputBorder(borderSide: new BorderSide(
              color: Colors.amber,
            )),
            focusedErrorBorder: UnderlineInputBorder(borderSide: new BorderSide(
              color: Colors.white
            )),
            helperText: widget.helperText,
            helperStyle: new TextStyle(color: Colors.white),
            errorStyle: new TextStyle(color: Colors.amber),
            icon: new Icon(widget.icon,color: Colors.white,),
        ),
      ),
    );
  }

}