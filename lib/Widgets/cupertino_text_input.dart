import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


class CupertinoCustomTextField extends StatefulWidget {
  final String label;
  final double labelFontSize;
  final double betweenSpace;
  final String hint;
  final double inputFontSize;
  final dynamic initialValue;
  final Function onSave;
  final validator;
  final TextInputType keyboardType;
//  final Function validate;
  const CupertinoCustomTextField({
    Key key,
    this.label = '',
    this.hint = '',
    this.labelFontSize = 14,
    this.betweenSpace = 5,
    this.inputFontSize = 14,
    this.initialValue = '',
    this.onSave,
    this.validator,
    this.keyboardType = TextInputType.text,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => CupertinoCustomTextFieldState();
}

class CupertinoCustomTextFieldState extends State<CupertinoCustomTextField> {

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(top: 20),
      child: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(left: 5),
              child: new Text(
                widget.label,
                style: new TextStyle(fontSize: widget.labelFontSize),
              ),
            ),
            new SizedBox(height: widget.betweenSpace,),
            new Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25))
              ),
              child: new TextFormField(
                keyboardType: widget.keyboardType,
                onSaved: widget.onSave,
                validator: widget.validator,
                initialValue: widget.initialValue == null ? '' : widget.initialValue.toString(),
                style: new TextStyle(fontSize: widget.inputFontSize),
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.all(Radius.circular(25)),
                  ),
                  contentPadding: EdgeInsets.symmetric(vertical: 9,horizontal: 15),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: .2,color: Colors.grey),
                    borderRadius: new BorderRadius.all(Radius.circular(25)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: .3,color: Colors.blue),
                    borderRadius: new BorderRadius.all(Radius.circular(25)),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }



}

