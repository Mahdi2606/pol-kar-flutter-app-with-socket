import 'package:flutter/material.dart';
import 'package:socket_test/Widgets/bottom_center_button.dart';
import 'package:socket_test/Widgets/custom_text_input.dart';


class RegisterForm extends StatefulWidget {
  final formKey;
  final usernameOnSave;
  final passwordOnSave;
  final nameOnSave;
  final mobileOnSave;
  RegisterForm({@required this.formKey,@required this.passwordOnSave,@required this.usernameOnSave,@required this.mobileOnSave,@required this.nameOnSave});
  @override
  State<StatefulWidget> createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Padding(
        padding: EdgeInsets.only(left: 10 , right: 10 , bottom: 50),
        child: new Form(
          key: widget.formKey,
          child: new Column(
            children: <Widget>[
              new CustomTextInput(
                onSave : widget.nameOnSave,
                icon: Icons.text_fields,
                hint: 'Name',
                obscure: false,
                validator: (String value){
                  if(value.length <= 0){
                    return 'name can not be empty';
                  }
                },
              ),
              new CustomTextInput(
                onSave : widget.usernameOnSave,
                icon: Icons.perm_identity,
                hint: 'Username',
                obscure: false,
                validator: (String value){
                  if(value.length <= 0){
                    return 'username can not be empty';
                  }
                  if(value.length < 8){
                    return 'username can not be less than 8 characters';
                  }
                },
              ),
              new CustomTextInput(
                onSave : widget.passwordOnSave,
                icon: Icons.lock_outline,
                hint: 'Password',
                obscure: false,
                validator: (String value){
                  if(value.length <= 0){
                    return 'password can not be empty';
                  }
                  if(value.length < 6){
                    return 'password can not be less than 6 characters';
                  }
                },
              ),
              new CustomTextInput(
                onSave : widget.mobileOnSave,
                icon: Icons.phone_android,
                hint: 'Mobile',
                obscure: false,
                validator: (String value){
                  if(value.length <= 0){
                    return 'monile can not be empty';
                  }
                },
              ),
            ],
          ),
        )
      )
    );
  }

}

//new PhonePicker(
//onSave : widget.mobileOnSave,
//icon: Icons.phone_iphone,
//hint: 'Mobile',
//obscure: false,
//validator: (String value){
//if(value.length <= 0){
//return 'mobile can not be empty';
//}
//},
//),