import 'package:flutter/material.dart';
import 'package:socket_test/Pagese/register_page.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/ChatPage/chat_page.dart';
import 'package:socket_test/Widgets/custom_text_input.dart';


class LoginForm extends StatefulWidget{
  final formKey;
  final usernameOnSave;
  final passwordOnSave;
  LoginForm({@required this.formKey,this.passwordOnSave,this.usernameOnSave});
  @override
  State<StatefulWidget> createState() => LoginFormState();
}


class LoginFormState extends State<LoginForm>  with AutomaticKeepAliveClientMixin<LoginForm>  {
  @override
  Widget build(BuildContext context) {
    return new Container(
//      color: ,
//      margin: EdgeInsets.only(bottom: 20),
      child: new Stack(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Form(
                  key: widget.formKey,
                  child: new Column(
                    children: <Widget>[
                      new CustomTextInput(
                        onSave : widget.usernameOnSave,
                        icon: Icons.person,
                        hint: 'Username',
                        obscure: false,
                        validator: (String value){
                          if(value.length <= 0){
                            return 'username can not be empty';
                          }
                        },
                      ),
                      new CustomTextInput(
                        onSave : widget.passwordOnSave,
                        icon: Icons.lock_outline,
                        hint: 'Password',
                        obscure: true,
                          validator: (String value){
                            if(value.length <= 0){
                              return 'password can not be empty';
                            }
                          },
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text('Do not have an account? ',style: new TextStyle(color: Colors.white),),
                            new GestureDetector(
                              child: new Text(' swipe to left',style: new TextStyle(color: Colors.amber),
                              ),
                              onTap : (){
//                                Navigator.of(context).pushNamed('/register');
  }
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      )
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}


