import 'package:flutter/material.dart';



class BottomButton extends StatelessWidget {


  @required final Widget text;
  @required final Function onPress;
  final Color color;
  BottomButton({this.text,this.onPress,this.color});


  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(bottom: 45),
      child: new Align(
          alignment: Alignment.bottomCenter,
          child: new RaisedButton(
            color: color,
            elevation: 1,
            onPressed: onPress,
            child: text,
          )
      ),
    );
  }

}