import 'package:flutter/material.dart';



class HeroImageView extends StatelessWidget {
  final String tag;
  final ImageProvider image;

  const HeroImageView({Key key, @required this.tag, @required this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body:  new Center(
        child: new Container(
          child: new Hero(
              tag: tag,
              child: new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: image,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              )
          ),
        ),
      ),
    );
  }

}