import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/model/user_model.dart';


class AppModel extends Model {
  int _counter = 1;
  int get counter => _counter;
  UserModel _currentUser;
  UserModel get currentUser => _currentUser;

  void incriment(){
    _counter++;
    notifyListeners();
  }

  void setLoginUser(UserModel user){
    _currentUser = user;
    notifyListeners();
  }



}