import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/Pagese/profile_edit_page.dart';
import 'package:socket_test/Test/TestPageTwo.dart';
import 'package:socket_test/scoped_model/AppModel.dart';
import 'package:flutter/cupertino.dart';

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new ScopedModelDescendant<AppModel>(
          builder: (context , child , model) => new Text(model.counter.toString()),
        ),
      ),
    );
  }

}
