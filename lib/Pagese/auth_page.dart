import 'package:flutter/material.dart';
import 'package:socket_test/Pagese/login_page.dart';
import 'package:socket_test/Pagese/register_page.dart';

class AuthPage extends StatelessWidget {
  final _controller = PageController(
      initialPage: 0
  );
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new PageView(
          controller: _controller,
          children: <Widget>[
            LoginPage(),
            RegisterPage()
          ],
        )
    );
  }

}