import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/Widgets/cupertino_text_input.dart';
import 'package:socket_test/Widgets/hero_image_view.dart';
import 'package:socket_test/scoped_model/AppModel.dart';



class ProfileEditPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ProfileEditPageState();
}


class ProfileEditPageState extends State<ProfileEditPage> {
  final _formKey = GlobalKey<FormState>();
  final _pageKey = GlobalKey<ScaffoldState>();
  String _swipeDirection = "Swipe";
  String _username;
  String _email;
  String _firstName;
  String _lastName;

  _onSaveUsername(String value){
    setState(() {
      _username = value;
    });
  }
  _onSaveFirstName(String value){
    setState(() {
      _firstName = value;
    });
  }
  _onSaveEmail(String value){
    setState(() {
      _email = value;
    });
  }
  _onSaveLastName(String value){
    setState(() {
      _lastName = value;
    });
  }

  String _usernameValidator(String value){
    if(value.length <= 0){
      return 'username can not be empty';
    }
    if(value.length < 8){
      return 'username can not be less than 8 characters';
    }
  }
  
  String _emailValidator(String value){

  }
  
  String _firstNameValidator(String value){

  }
  
  String _lastNameValidator(String value){

  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return new ScopedModelDescendant<AppModel>(
      builder: (BuildContext context , Widget child , AppModel model){
        return new Scaffold(
          key: _pageKey,
          body: SingleChildScrollView(
            child: new Container(
              height: height,
              child: new Column(
                children: <Widget>[
                  new Stack(
                    children: <Widget>[
                      new Container(
                        height: 200,
                        width: width,
                        child: new Stack(
                          children: <Widget>[
                            new Container(
                              width: width,
                              height: 150,
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.only(bottomLeft: Radius.elliptical(200, 50),bottomRight: Radius.elliptical(200, 50)),
                                gradient: LinearGradient(
                                  colors: <Color>[Color(0xFF2849FF),Color(0xFF6BA0F4),Color(0xFF0fBED0)]
                                )
                              ),
                            ),
                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Row(
                                children: <Widget>[
                                  new Padding(
                                    padding: EdgeInsets.only(left: 20),
                                    child: new GestureDetector(
                                      onTap: (){
                                        _showDialog(model.currentUser.profile.toString());
                                      },
                                      child: new Hero(
                                        tag: 'profileImage',
                                        child: new Container(
                                          width: 120,
                                          height: 120,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(70)),
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                'http://192.168.43.3:2000/files/${model.currentUser.profile}'
                                              ),
                                              fit: BoxFit.cover
                                            )
                                          ),
                                        ),
                                      )
                                    )
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(left: 10,top: 5),
                                    child: new Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(30)),
                                        color: Color(0xFF0fBED0)
                                      ),
                                      child: new Center(
                                        child: new IconButton(
                                          icon: Icon(AntDesign.getIconData('camerao')),
                                          onPressed: (){},
                                        )
                                      ),
                                    ),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(left: 10,bottom: 0),
                                    child: new Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(30)),
                                        color: Colors.greenAccent
                                      ),
                                      child: new Center(
                                        child: IconButton(
                                          icon: Icon(Ionicons.getIconData('ios-images')),
                                          onPressed: (){
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(left: 10,bottom: 20),
                                    child: new Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(30)),
                                        color: Colors.deepOrangeAccent
                                      ),
                                      child: new Center(
                                        child: IconButton(
                                          icon: Icon(Ionicons.getIconData('ios-settings')),
                                          onPressed: (){
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 20 , horizontal: 10),
                    child: new Container(
                      width: width,
                      child: new Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: new Form(
                          key: _formKey,
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              CupertinoCustomTextField(
                                label: 'Username',
                                betweenSpace: 5,
                                labelFontSize: 16,
                                initialValue: model.currentUser.username,
                                onSave: _onSaveUsername,
                                validator: (String value) => _usernameValidator(value),
                              ),
                              CupertinoCustomTextField(
                                label: 'Email',
                                betweenSpace: 5,
                                labelFontSize: 16,
                                initialValue: model.currentUser.email,
                                keyboardType: TextInputType.emailAddress,
                                onSave: _onSaveEmail,
                                validator: _emailValidator,
                              ),
                              CupertinoCustomTextField(
                                label: 'FirstName',
                                betweenSpace: 5,
                                labelFontSize: 16,
                                initialValue: model.currentUser.name,
                                onSave: _onSaveFirstName,
                                validator: (String value) => _firstNameValidator(value),
                              ),
                              CupertinoCustomTextField(
                                label: 'LastName',
                                betweenSpace: 5,
                                labelFontSize: 16,
                                initialValue: model.currentUser.family,
                                onSave: _onSaveLastName,
                                validator: (String value) => _lastNameValidator(value),
                              ),
                            ],
                          ),
                        )
                      )
                    ),
                  )
                ],
              ),
            )
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: updateUser,
            child: new Icon(Icons.check),
          ),
          floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        );
      },
    );
  }

  _showDialog(String image) async {
    return showDialog(
      context: context,
      builder: (_){
        return new AlertDialog(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.all(0),
          titlePadding: EdgeInsets.all(0),
          shape: CircleBorder(),
          content: new Container(
            width: 300,
            height: 300,
            decoration: new BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(300)),
              image: DecorationImage(
                image: NetworkImage(
                  'http://192.168.43.3:2000/files/$image'
                ),
                fit: BoxFit.fitWidth
              )
            ),
          ),
        );
      }
    );
  }

  updateUser() async {
    _formKey.currentState.save();
    if(_formKey.currentState.validate()){

      Map data = {
        "name" : _firstName.length <=0 ? null : _firstName,
        "username" : _username,
        "email" : _email.length <= 0 ? null: _email ,
        "family" : _lastName.length <= 0 ? null : _lastName
      };
      Map response = await new UserController().updateUser(data);
      if(response['status']){
        ScopedModel.of<AppModel>(context).setLoginUser(response['currentUser']);
        Navigator.pop(context);
      }else{
        GlobalClass.showFlushBar(context, 'Update false');
      }

    }else{
      _formKey.currentState.validate();
    }
  }
}
