
import 'package:flutter/material.dart';
import 'package:socket_test/Classes/auth_controller.dart';
import 'package:socket_test/Pagese/home_screens/home_page.dart';
import 'package:socket_test/Widgets/bottom_center_button.dart';
import 'package:socket_test/Widgets/login_form.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  final pageKey = GlobalKey<ScaffoldState>();
  String username;
  String password;

  usernameOnSave(String value){
    username = value;
  }
  passwordOnSave(String value){
    password = value;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return new Scaffold(
      key: pageKey,
      resizeToAvoidBottomPadding: false,
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            colors: [new Color(0xAAFF0000),new Color(0xAA0000FF)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          )
        ),
        child: new Stack(
          children: <Widget>[
            new LoginForm(
              formKey : formKey,
              usernameOnSave : usernameOnSave,
              passwordOnSave : passwordOnSave,
            ),
            new BottomButton(
              color: Colors.white,
              text: new Text('Login'),
              onPress: () async {
                if(formKey.currentState.validate()){
                  formKey.currentState.save();
                  Map response = await new AuthController().login({'username' : username , 'password' : password});
                  if(response['status']){
                    setUserData(response['data']);
                  }else{
                    pageKey.currentState.showSnackBar(
                      new SnackBar(
                        backgroundColor: Colors.black,
                        duration: new Duration(
                          milliseconds: 500
                        ),
                        content: new Text('اطلاعات وارد شده صحیح نیست'),
                      )
                    );
                  }
                }else{
                  formKey.currentState.validate();
                }
              },
            )
          ],
        ),
      )
    );
  }


  setUserData(Map data) async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    perfs.setString('user_api_token', data['token']);
    Navigator.pushReplacementNamed(context, '/');
  }
}