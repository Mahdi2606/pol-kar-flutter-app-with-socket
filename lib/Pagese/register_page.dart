
import 'package:flutter/material.dart';
import 'package:socket_test/Classes/auth_controller.dart';
import 'package:socket_test/Widgets/register_form.dart';
import 'package:socket_test/Widgets/bottom_center_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final registerFormKey = GlobalKey<FormState>();
  final registerPageKey = GlobalKey<ScaffoldState>();
  String username;
  String password;
  String name;
  String mobile;

  usernameOnSave(String value){
    username = value;
  }
  passwordOnSave(String value){
    password = value;
  }
  nameOnSave(String value){
    name = value;
  }
  mobileOnSave(String value){
    mobile = value;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return new Scaffold(
        key: registerPageKey,
//        resizeToAvoidBottomPadding: false,
        body: new Container(
          decoration: new BoxDecoration(
              gradient: new LinearGradient(
                colors: [new Color(0xAAFF0000),new Color(0xAA0000FF)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              )
          ),
          child: new Stack(
            children: <Widget>[
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new RegisterForm(
                      formKey: registerFormKey,
                      passwordOnSave: passwordOnSave,
                      usernameOnSave: usernameOnSave,
                      mobileOnSave: mobileOnSave,
                      nameOnSave: nameOnSave
                  )
                ],
              ),
              new BottomButton(
                text: new Text('Register'),
                color: Colors.white,
                onPress: () async {
                  registerFormKey.currentState.save();
                  if(registerFormKey.currentState.validate()){

                    Map response = await new AuthController().register(
                        {'username' : username , 'password' : password , 'phone' : mobile , 'name' : name});
                    if(response['status']){
                        Map login = await new AuthController().login(
                          {'username' : username , 'password' : password}
                        );
                        if(login['status']){
                          setUserData(login['data']);
                        }else{
                          registerPageKey.currentState.showSnackBar(
                              new SnackBar(
                                backgroundColor: Colors.black,
                                duration: new Duration(
                                    milliseconds: 500
                                ),
                                content: new Text('ثبت نام با موفقیت انجام شد ، لاگین کنید'),
                              )
                          );
                        }
                    }else{
                      registerPageKey.currentState.showSnackBar(
                          new SnackBar(
                            backgroundColor: Colors.black,
                            duration: new Duration(
                                milliseconds: 500
                            ),
                            content: new Text('خطا در ثبت نام'),
                          )
                      );
                    }
                  }else{
                    registerFormKey.currentState.validate();
                  }
                },
              )
            ],
          ),
        )
    );
  }
  setUserData(Map data) async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    perfs.setString('user_api_token', data['token']);
    Navigator.pushReplacementNamed(context, '/');
  }
}


