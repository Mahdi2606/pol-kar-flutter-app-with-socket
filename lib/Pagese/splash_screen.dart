import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:socket_test/Classes/Navigate.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_test/Classes/auth_controller.dart';
import 'package:socket_test/Test/test.dart';
import 'package:socket_test/scoped_model/AppModel.dart';
import 'package:scoped_model/scoped_model.dart';


class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {

  final splashKey = GlobalKey<ScaffoldState>();
    startTime() async {
      var _duration = new Duration(seconds: 2);
       Timer(_duration , await checkLogin());
    }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: splashKey,
//      backgroundColor: Colors.blue,
        body: new Center(
          child: CupertinoActivityIndicator(),
        )
    );
  }

  checkLogin() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    String token = perfs.getString('user_api_token');
    if (token == null) {
      Navigator.of(context).pushReplacementNamed('/auth');
    } else {
      bool connection = await GlobalClass.checkConnection();
      if (connection) {
        var response = await new AuthController().checkApi(token);
        if (response == null) {
          Navigator.of(context).pushReplacementNamed('/auth');
        } else {
          ScopedModel.of<AppModel>(context).setLoginUser(response['currentUser']);
          Navigate.navigateToHomeScreen(context);
        }
      } else {
        splashKey.currentState.showSnackBar(
            new SnackBar(
            backgroundColor: Colors.redAccent,
            duration: new Duration(hours: 2),
            content: new GestureDetector(
              onTap: (){
                splashKey.currentState.hideCurrentSnackBar();
                checkLogin();
              },
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text('please check the internet connection'),
                  new Icon(Icons.wifi_lock)
                ],
              ),
            ),
          )
        );
      }
    }
  }
}
