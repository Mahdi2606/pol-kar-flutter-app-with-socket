import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/Pagese/home_screens/add_page.dart';
import 'package:socket_test/Pagese/home_screens/favourite_page.dart';
import 'package:socket_test/Pagese/home_screens/home_page.dart';
import 'package:socket_test/Pagese/home_screens/profile_page.dart';
import 'package:socket_test/Pagese/home_screens/search_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/model/user_model.dart';
import 'package:socket_test/scoped_model/AppModel.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  String currentPageName = 'home';
  TabController tabController;
  var _selectedTabIndex = 0;
  final Color _selectedTabColor = Colors.blue;
  final Color _unSelectedTabColor = Colors.black;
  @override
  void initState() {
    super.initState();
    tabController = new TabController(
      length: 5,
      vsync: this,
      initialIndex: 0,
    );
    tabController.addListener(() {
      setState(() {
        _selectedTabIndex = tabController.index;
      });
    });

  }

  Widget tabBar(width) {
    return new SizedBox(
      width: width,
      height: 70,
      child: new Stack(
        children: <Widget>[
          new Align(
            alignment: Alignment.bottomCenter,
            child: new SizedBox(
                width: width,
                height: 50,
//      color: Color(0x00FFFFFF),
                child: new BackdropFilter(
                  filter: ImageFilter.blur(sigmaY: 10 , sigmaX: 10),
                  child: new Container(
                      color: Color(0x55FFFFFF),
                      child : new Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: new Center(
                            child: new Stack(
                              children: <Widget>[
                                new Align(
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                        width: width/3,
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            new Flexible(
                                              child: new GestureDetector(
                                                  onTap: () => tabController.animateTo(0),
                                                  child: new Container(
                                                    alignment: Alignment(0, 0),
                                                    child: new Icon(AntDesign.getIconData('home'),color: _selectedTabIndex == 0 ? _selectedTabColor : _unSelectedTabColor),
                                                  )
                                              ),
                                            ),
                                            new Flexible(
                                              child: new GestureDetector(
                                                  onTap: () => tabController.animateTo(1),
                                                  child: new Container(
                                                    alignment: Alignment(0, 0),
                                                    child: new Icon(AntDesign.getIconData('search1'),color: _selectedTabIndex == 1 ? _selectedTabColor : _unSelectedTabColor),
                                                  )
                                              ),
                                            ),
                                          ],
                                        )
                                    )
                                ),
                                new Align(
                                    alignment: Alignment.centerRight,
                                    child: new Container(
                                        width: width/3,
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            new Flexible(
                                              child: new GestureDetector(
                                                  onTap: () => tabController.animateTo(3),
                                                  child: new Container(
                                                    alignment: Alignment(0, 0),
                                                    child: new Icon(AntDesign.getIconData('hearto'),color: _selectedTabIndex == 3 ? _selectedTabColor : _unSelectedTabColor),
                                                  )
                                              ),
                                            ),
                                            new Flexible(
                                              child: new GestureDetector(
                                                  onTap: () => tabController.animateTo(4),
                                                  child: new Container(
                                                    alignment: Alignment(0, 0),
                                                    child: new Icon(AntDesign.getIconData('user'),color: _selectedTabIndex == 4 ? _selectedTabColor : _unSelectedTabColor),
                                                  )
                                              ),
                                            )
                                          ],
                                        )
                                    )
                                ),
                              ],
                            )
                        ),
                      )
                  ),
                )
            ),
          ),
          new Align(
            alignment: Alignment.center,
            child: new Padding(padding: EdgeInsets.only(bottom: 10),child:  new GestureDetector(
              onTap: () => tabController.animateTo(2),
              child: new ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                child: new BackdropFilter(
                  filter: ImageFilter.blur(sigmaY: 10 ,sigmaX: 10),
                  child: new Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: <Color>[Colors.deepPurple,Colors.blue]
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(30)),
//                      border: Border.all(width: 2 , color: Colors.black54),
                        color: Colors.white70
                    ),
                    child: new Icon(AntDesign.getIconData('plus'),color: Colors.black),
                  ),
                ),
              ),
            ),)
          ),
        ],
      ),
    );
  }

  Widget tabBarView(UserModel currentUser){
    return new TabBarView(
      physics: NeverScrollableScrollPhysics(),
      controller: tabController,
      children: <Widget>[
        new HomePage(tabController : tabController),
        new SearchPage(tabController : tabController),
        new AddPage(tabController : tabController),
        new FavouritePage(tabController : tabController),
        new ProfilePage(currentUser : currentUser,tabController : tabController),
    ]);
  }

  Drawer drawer = new Drawer(
    elevation: 10,
    child: new Column(
      children: <Widget>[
        new UserAccountsDrawerHeader(accountName: new Text('classic2326'), accountEmail: new Text('mahdob999@gmail.com'), decoration: new BoxDecoration(
            gradient: LinearGradient(colors: [Colors.redAccent , Colors.blueAccent])
        ),
            currentAccountPicture: new Container(
              decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  border: Border.all(width: 2),
                  image: DecorationImage(image: NetworkImage('https://www.gravatar.com/avatar/47c7f0f57d29c580fab6917d626412b4?s=150&d=mm&r=g'))
              ),

            )

        )
      ],
    ),
  );
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return new ScopedModelDescendant<AppModel>(
      builder: (context , child , model){
        return new Scaffold(
          body: new Stack(
            children: <Widget>[
              tabBarView(model.currentUser),
              new Align(
                alignment: Alignment.bottomCenter,
                child: new Container(
                  width: width,
                  height: 70,
                  color: Colors.transparent,
                  child: new Center(
                    child: tabBar(width),
                  )
                  ,
                )
              ),
            ],
          ),
        );
      }
    );
  }
/*



 */
  setUser() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    String user = perfs.getString('login_user');
    print(user);

  }
}
