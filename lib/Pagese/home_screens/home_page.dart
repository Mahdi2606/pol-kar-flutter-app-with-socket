import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/Classes/post_controller.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/ChatBar/contactBar.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/ChatPage/chat_page.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_box.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_test/Widgets/posts_widget.dart';
import 'package:socket_test/model/post_model.dart';


class HomePage extends StatefulWidget {
  final tabController;
  const HomePage({Key key, this.tabController}) : super(key: key);
  @override
  State<StatefulWidget> createState() => HomePageState();

}

class HomePageState extends State<HomePage> with AutomaticKeepAliveClientMixin<HomePage> {
  final homePageKey = GlobalKey<ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  List<PostModel> _posts = [];
  bool _spinner = true;
  int _currentPage = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getPosts();
    _scrollController.addListener((){
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      if(currentScroll > maxScroll - 1){
        _getPosts(page: _currentPage + 1);
      }
    });

    if (!mounted) return;
  }



  _getPosts({ int page = 1 , bool refresh = false }) async {
    if(refresh) _posts.clear();
    Map posts = await new PostController().getFollowedUsersPosts(page);
    setState(() {
      _posts.addAll(posts['posts']);
      _spinner = false;
      _currentPage = posts['currentPage'];
    });
  }
  Future<Null> _onRefresh() async {
    setState(() {
      _spinner = true;
    });
    await _getPosts(refresh: true);

    return null;
  }


  @override
  Widget build(BuildContext context) {
    Widget appBar() {
      return new SliverAppBar(
        pinned: false,
        snap: true,
        floating: true,
        centerTitle: true,
        backgroundColor: new Color(0xffffffff),
        elevation: 2,
        leading: new FlatButton(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          shape: CircleBorder(),
          onPressed: logout,
          child: new Icon(Icons.exit_to_app),
        ),
        title: SizedBox(
          height: 40.0,
          child: new Center(child: new Text('PolKar'),)
        ),
        actions: <Widget>[
          new FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              shape: CircleBorder(),
              onPressed: (){print('Chat');},
              child:  new Icon(Icons.chat_bubble_outline)
          )
        ],
      );
    }

    return Scaffold(
      key: homePageKey,
//      drawer: ChatPage(),
      body: new NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
          return [
            appBar()
          ];
        },
        body: Center(
            child: _spinner
                ? new RefreshProgressIndicator()
                :_posts.length > 0
                  ? PostWidget.postList(_posts, _onRefresh)
                  : new Center(
                      child: new GestureDetector(
                        onTap: _onRefresh,
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(Ionicons.getIconData('ios-radio-button-on')),
                            new SizedBox(width: 10,),
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text('No post yet'),
                                new SizedBox(height: 5,),
                                new Text('Tap to refresh'),
                              ],
                            )
                          ],
                        ),
                      )
                    ),
        ),
      )
    );
  }


  logout() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    await perfs.remove('user_api_token');
    Navigator.of(context).pushReplacementNamed('/auth');
  }


  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
