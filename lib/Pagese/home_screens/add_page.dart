import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:path/path.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/Pagese/widgets/image_view.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:dio/dio.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';

  class AddPage extends StatefulWidget {
    final TabController tabController;
    const AddPage({Key key, this.tabController}) : super(key: key);
  @override
  State<StatefulWidget> createState() => AddPageState();
  }


  class AddPageState extends State<AddPage>  {
    final formKey = GlobalKey<FormState>();
    final addPageKey = GlobalKey<ScaffoldState>();
    bool _pin = false;
    bool _comment = true;
    bool _subjectErr = false;
    List _images = [];
    List<Map> _uploads = [];
    String _subject;
    String _text;
    bool _spinnr = false;
    double _postProgress = 0;
    onSaveSubject(String value){
      _subject = value;
    }

    onSaveText(String value){
      _text = value;
    }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return new Scaffold(
      key: addPageKey,
      body: new SingleChildScrollView(
        child: new Stack(
          children: <Widget>[

            new Column(
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: new Container(
//              color: new Color(0x55000000),
                      child: new Padding(
                        padding: EdgeInsets.symmetric(vertical: 10 , horizontal: 5),
                        child: new Container(
                          height: 250,
//                          child: ListView.builder(
//                            itemCount: _images.length + 1,
//                            scrollDirection: Axis.horizontal,
//                            itemBuilder: (context , index){
//                              if(index == _images.length){
//                                return new GestureDetector(
//                                  child: Container(
//                                    width: 60,
//                                    decoration: new BoxDecoration(
//                                      color: Color(0x11000000),
//                                      borderRadius: BorderRadius.all(Radius.circular(40)),
//                                    ),
//                                    child: new Icon(Icons.add_a_photo,size: 20,),
//                                  ),
//                                  onTap: pickImageFromGallery,
//                                );
//                              }else{
//                                return new Padding(
//                                  padding: EdgeInsets.only(right: 5),
//                                  child: new Hero(
//                                    tag: index.toString(),
//                                    child: new GestureDetector(
//                                      onTap: (){
//                                        Navigator.push(context, new MaterialPageRoute(builder: (context)=> new ImageView(files: _images,imageIndex : index)));
//                                      },
//                                      child: new Container(
//                                        width: 60,
//                                        decoration: new BoxDecoration(
//                                            borderRadius: BorderRadius.all(Radius.circular(40)),
//                                            color: Colors.teal,
//                                            image: new DecorationImage(
//                                                image: new FileImage(_images[index]),
//                                                fit: BoxFit.cover
//                                            )
//                                        ),
//                                      ),
//                                    )
//                                  )
//                                );
//                              }
//                            },
//                          ),
                        child: new Swiper(
                          itemCount: _images.length + 1,
                          viewportFraction: .8,
                          scale: .9,
                          loop: false,
                          itemBuilder: (BuildContext context, int index) {
                            if(index == _images.length){
                              return new Container(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new Padding(
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: new GestureDetector(
                                          child: Container(
                                            decoration: new BoxDecoration(
                                              gradient: LinearGradient(colors: [Color(0xFF0AB4C4) , Color(0xFF1A7899)]),
                                              borderRadius: BorderRadius.all(Radius.circular(5)),
                                            ),
                                            child: new Icon(Ionicons.getIconData('ios-camera'),size: 30,),
                                          ),
                                          onTap: pickImageFromCamera,
                                        ),
                                      ),
                                      flex: 1,
                                      fit: FlexFit.tight,
                                    ),
                                    new Flexible(
                                      child: new Padding(
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: new GestureDetector(
                                          child: Container(
                                            decoration: new BoxDecoration(
                                              gradient: LinearGradient(colors: [Color(0xFF0AB4C4) , Color(0xFF1A7899)]),
                                              borderRadius: BorderRadius.all(Radius.circular(5)),
                                            ),
                                            child: new Icon(Ionicons.getIconData('ios-images'),size: 30,),
                                          ),
                                          onTap: pickImageFromGallery,
                                        ),
                                      ),
                                      flex: 1,
                                      fit: FlexFit.tight,
                                    ),
                                  ],
                                ),
                              );
                            }else{
                              return new Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child:new GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, new MaterialPageRoute(builder: (context)=> new ImageView(files: _images,imageIndex : index)));
                                  },
                                  child : new Container(
                                    width: 60,
                                    decoration: new BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        color: Colors.teal,
                                        image: new DecorationImage(
                                            image: new FileImage(_images[index]),
                                            fit: BoxFit.cover
                                        )
                                    ),
                                  ),
                                )
                              );
                            }
                          },

                        ),
                        ),
                      )
                  ),
                ),
                new Padding(
                  padding: EdgeInsets.only(left: 10 , right: 10 , top: 10),
                  child: new Form(
                    key: formKey,
                    child: new Column(
                      children: <Widget>[
                        _pin
                            ?  new Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(color: Colors.grey, spreadRadius: .2 ),
                              ]
                          ),
                          child: new TextFormField(
                            onSaved: onSaveSubject,
                            validator: (String value) {
                              if(value.length <= 0){
                                GlobalClass.showFlushBar(context,'Subject can not be empty');
                                return '';
                              }
                            } ,
                            decoration: InputDecoration(

                              icon: new Icon(Icons.subject , color: _subjectErr ? Colors.red : Colors.grey,),
                              hintText: 'subject',
                              border : InputBorder.none,
                            ),
                          ),
                        )
                            : new SizedBox(
                        ),
                        new Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: new Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  new BoxShadow(color: Colors.grey, spreadRadius: .2 ),
                                ]
                            ),
                            child: new Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: new TextFormField(
                                validator: (String value){
                                  if(_images.length <= 0 && value.length <=0){
                                    GlobalClass.showFlushBar(context,'Text and image can not be empty at the same time');
                                    return '';
                                  }
                                },
                                onSaved: onSaveText,
                                maxLines: 5,
                                decoration: InputDecoration(
                                  hintText: 'text',
                                  border : InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),
                        new ExpansionTile(
                          leading: new Icon(Ionicons.getIconData('ios-settings')),
                          trailing: new Icon(Icons.arrow_drop_down),
                          title: new Text('post option'),
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Padding(
                                  padding: EdgeInsets.only(left: 10 , right: 10 , bottom: 10),
                                  child: new Container(
                                    width: 50.0,
                                    height: 50.0,
                                    child: new RawMaterialButton(
                                      shape: new CircleBorder(),
                                      elevation: 0.0,
                                      highlightElevation: 0,
                                      disabledElevation: 0,
                                      fillColor: _pin ? new Color(0xFF4FC3F7) :  new Color(0x000000),
                                      child: new Icon(
                                          Icons.attach_file,
                                          color : _pin ? Colors.white : Colors.black,
                                      ), onPressed: () {
                                      setState(() {
                                        _pin = !_pin;
                                        _subject = null;
                                      });
                                    },
                                    ),
                                  ),
                                ),
                                new Padding(
                                  padding: EdgeInsets.only(left: 10 , right: 10 , bottom: 10),
                                  child: new Container(
                                    width: 50.0,
                                    height: 50.0,
                                    child: new RawMaterialButton(
                                      shape: new CircleBorder(),
                                      elevation: 0.0,
                                      highlightElevation: 0,
                                      disabledElevation: 0,
                                      fillColor: _comment ? new Color(0xFFE91E63) :  new Color(0x000000),
                                      child: new Icon(
                                        Icons.message,
                                        color: _comment ? Colors.white : Colors.black,
                                      ), onPressed: () {
                                      setState(() {
                                        _comment = !_comment;
                                      });
                                    },
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            _spinnr ? new Align(
              alignment: Alignment.center,
              child: new Container(
                width: width,
                height: height,
                color: Colors.black87,
                child: new Center(
                  child: CircularPercentIndicator(
                    animateFromLastPercent: true,
                    animationDuration: 3,
                    radius: 70,
                    lineWidth: 1,
                    percent: _postProgress / 100,
                    center: new Text('${_postProgress.toInt()}%' , style: TextStyle(color: Colors.white),),
                  ),
                ),
              ),
            ) :
                new SizedBox()
          ],
        ),
      ),
      floatingActionButton: new Padding(
        padding: EdgeInsets.only(bottom: 40),
        child: FloatingActionButton(
          onPressed: sendPost,
          backgroundColor: Colors.blue,
          child: new Center(
            child: new Icon(Ionicons.getIconData('ios-paper-plane')),
          ),
        ),
      )
    );
  }

    pickImageFromGallery() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);

      File cropImage = await _cropImage(image);
      var data = cropImage.readAsBytesSync();
      var base = base64.encode(data);

      Map upload = {
        "data" : base,
        "name" : basename(image.path),
        "size" : image.lengthSync(),
        "type" : extension(image.path),
      };


      setState(() {
        _images.add(cropImage);
        _uploads.add(upload);
      });

    }

    Future<File> _cropImage(File imageFile) async {
      File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        ratioX: 1.2,
        ratioY: 1,
        circleShape: true,
        toolbarTitle: 'Crop Photo',
        maxWidth: 1024,
        maxHeight: 720,
      );
      return croppedFile;
    }

    pickImageFromCamera() async {
      var image = await ImagePicker.pickImage(source: ImageSource.camera);

      File cropImage = await _cropImage(image);
      var data = cropImage.readAsBytesSync();
      var base = base64.encode(data);

      Map upload = {
        "data" : base,
        "name" : basename(image.path),
        "size" : image.lengthSync(),
        "type" : extension(image.path),
      };


      setState(() {
        _images.add(cropImage);
        _uploads.add(upload);
      });


    }
    sendPost() async {
      if(formKey.currentState.validate()){
        formKey.currentState.save();
        var dio = Dio();
        var body = json.encode({
          'medias' : _uploads,
          'text' : _text,
          'subject' : _subject,
          'pin' : _pin,
          'commentMode' : _comment,
        });

        String token = await UserController.getToken();
        Response response = await dio.post(
          'http://192.168.43.3:2000/v1/post?token=$token',
          data: body,
          onSendProgress: (int sent, int total) {
            var progress = ((sent*100)/total).toInt();
            setState(() {
              _spinnr = true;
              _postProgress = progress.toDouble();
            });
          },
        );

        if(response.data['status']){
          widget.tabController.animateTo(0);
          setState(() {
            _spinnr = false;
            _subject = null;
            _text = null;
            _comment = true;
            _pin = false;
            _images = [];
            _uploads = [];
          });

        }else{
          _spinnr = false;
          GlobalClass.showFlushBar(context, 'Error while sending post');
        }

      }else{
        print('ERR');
      }

    }

    Future<List<int>> testCompressFile(File file) async {
      var result = await FlutterImageCompress.compressWithFile(
        file.absolute.path,
        quality: 100,
      );
      print(file.lengthSync());
      print(result.length);
      return result;
    }

  }

