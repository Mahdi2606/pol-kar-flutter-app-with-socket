import  'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/Classes/Navigate.dart';
import 'package:socket_test/Classes/post_controller.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/Pagese/home_screens/profile_screens/post_book_mark_screen.dart';
import 'package:socket_test/Pagese/home_screens/profile_screens/post_grade_view_screen.dart';
import 'package:socket_test/Pagese/home_screens/profile_screens/post_pins_screen.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:socket_test/model/user_model.dart';
import 'package:socket_test/Widgets/posts_widget.dart';
import 'package:socket_test/scoped_model/AppModel.dart';
import 'package:flutter_svg/flutter_svg.dart';

  class ProfilePage extends StatefulWidget {
    final UserModel currentUser;
    final tabController;
  ProfilePage({this.tabController, @required this.currentUser});
  @override
  State<StatefulWidget> createState() => ProfilePageState();
  }

  class ProfilePageState extends State<ProfilePage> with SingleTickerProviderStateMixin{
    ScrollController _scrollController = new ScrollController();
    TabController _tabController;
    bool _spinner = false;
    int _currentPage;
    List<PostModel> _posts = [];
    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      _tabController = new TabController(length: 4, vsync: this,initialIndex: 0);
      _givePosts();
      _scrollController.addListener((){
        double _maxScroll = _scrollController.position.maxScrollExtent;
        double _currentScroll = _scrollController.position.pixels;
        if(_currentScroll > _maxScroll - 1){
          _givePosts(page: _currentPage + 1);
        }
      });
    }

    Future _givePosts({ int page : 1 , bool refresh = false}) async {
      if(refresh) _posts.clear();
      Map posts = await new PostController().getUserPosts(widget.currentUser.username.toString() , page );
      setState(() {
        _posts.addAll(posts['posts']);
        _currentPage = posts['currentPage'];
      });
    }


    Widget sliverTabBar(){
      return new SliverAppBar(
        backgroundColor: Colors.white,
        expandedHeight: 130,
        flexibleSpace: FlexibleSpaceBar(
          background: new Container(
            decoration: new BoxDecoration(
              color: new Color(0x77E3F2FD),
            ),
            child: new Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: new Stack(
                children: <Widget>[
                  new Align(
                      alignment: Alignment.centerLeft,
                      child: new Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new GestureDetector(
                                onVerticalDragUpdate: (DragUpdateDetails details) {
                                  print(details);
                                },
                                child: new Container(
                                  height: 100,
                                  width: 100,
                                  decoration: new BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(
                                        'http://192.168.43.3:2000/files/${widget.currentUser.profile.toString()}',
                                      ),
                                      fit: BoxFit.cover
                                    )
                                  ),
                                ),
                                onTap: (){
                                  //nothing
                                },
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 5),
                                child: new Text(widget.currentUser.name.toString()),
                              )
                            ],
                          ),
                          new Expanded(
                            child: new Stack(
                              children: <Widget>[
                                new Align(
                                  alignment: Alignment.centerRight,
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Container(
                                          child: new Padding(
                                            padding: EdgeInsets.only(top: 20),
                                            child: new Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    new Text('Follower' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                    new SizedBox(height: 8,),
                                                    new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                  ],
                                                ),
                                                new Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    new Text('Following' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                    new SizedBox(height: 8,),
                                                    new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )
                                      ),
                                      new GestureDetector(
                                        onTap: (){Navigate.navigateToProfileEditPage(context,transition: 'horizantal');},
                                        child: new Container(
                                            width: 150,
                                            height: 30,
                                            decoration: new BoxDecoration(
                                                color: Colors.grey[300],
                                                borderRadius: BorderRadius.all(Radius.circular(20))
                                            ),
                                            margin: EdgeInsets.only(top: 10),
                                            child: new Center(
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  new Icon(Ionicons.getIconData('ios-settings'), size: 20,) ,
                                                  SizedBox(width: 10,) ,
                                                  new Text('Settings')
                                                ],
                                              )
                                            )
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    Widget _tabItems(){
      return new Material(
        color: Colors.white,
        child: new TabBar(

          unselectedLabelColor: Colors.black,
          indicatorColor: Colors.blue,
          labelColor: Colors.blue,
          controller: _tabController,
          tabs: <Widget>[
            new Tab(icon: new Icon(Ionicons.getIconData('ios-list'))),
            new Tab(icon: new Icon(Ionicons.getIconData('ios-grid'))),
            new Tab(icon: new Icon(Ionicons.getIconData('ios-pricetags'))),
            new Tab(icon: new Icon(Ionicons.getIconData('md-bookmark'))),
          ],
        ),
      );
    }

    Widget _tabBarScreens() {
      return new TabBarView(
        children: [
        PostWidget.postList(_posts, _onRefresh),
        PostWidget.gridPost(_posts, _onRefresh),
        new PostPinScreen(),
        new PostBookMarkScreen(),
        ],
        physics: ClampingScrollPhysics(),
        controller: _tabController,);
    }

    @override
    Widget build(BuildContext context) {
      return new Scaffold(
        body: _spinner ? new Center(child: new CupertinoActivityIndicator(),) : new NestedScrollView(
          controller: _scrollController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
            return <Widget>[
              sliverTabBar()
            ];
          },
          body: new Stack(
            children: <Widget>[
              new Align(
                alignment: Alignment.topCenter,
                child: new Container(
                  child: _tabItems(),
                )
              ),
              new Padding(
                padding: EdgeInsets.only(top: 50),
                child: _tabBarScreens(),
              )
            ],
          )
        ),
      );
    }

    Future<Null> _onRefresh() async {
      await _givePosts(refresh: true);
      return null;
    }


  }