  import 'package:flutter/material.dart';
import 'package:socket_test/Classes/Navigate.dart';
import 'package:socket_test/Classes/search.dart';
import 'package:socket_test/Pagese/home_screens/search_screens/user_search.dart';


  class SearchPage extends StatefulWidget {
    final tabController;
    const SearchPage({Key key, this.tabController}) : super(key: key);
  @override
  State<StatefulWidget> createState() => SearchPageState();
  }


  class SearchPageState extends State<SearchPage> with SingleTickerProviderStateMixin {
    TabController _tabController;
    List users = [];
    int index = 0;
    void initState() {
      super.initState();
      _tabController = new TabController(
          length: 2,
          vsync: this,
          initialIndex: 0
      );
      _tabController.addListener(setTabBarIndex);
    }

    @override
    void dispose() {
      _tabController.dispose();
      super.dispose();
    }

    Widget tabBarItems(){
      return new TabBar(
        unselectedLabelColor: Colors.purpleAccent,
        controller: _tabController,
        tabs: <Widget>[
          new Tab(icon: new Icon(Icons.person_outline , color: Colors.black,),),
          new Tab(icon: new Icon(Icons.grid_on, color: Colors.black,),),
        ],
      );
    }

    Widget tabBarScreens(){
      return TabBarView(
        controller: _tabController,
        children: <Widget>[
          userSearchScreen(),
          new Container(color: Colors.purpleAccent),
        ],
      );
    }

    Widget userSearchScreen() {
      return Container(
        child: new ListView.builder(
          itemCount: users.length,
          itemBuilder: (context , index){
            return new ListTile(
              onTap: (){Navigate.navigateToUserPage(context , {'username' : users[index]['username'].toString()} , transition: 'horizantal');},
              dense: true,
              leading: new CircleAvatar(radius: 25,child: new Text(users[index]['username'][0].toString()),),
              title: new Text(users[index]['username'].toString()),
              subtitle: new Text('User Description'),
            );
          },
        ),
      );
    }

    @override
    Widget build(BuildContext context) {
      return new Scaffold(
        body: new NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
              return <Widget>[
                new SliverAppBar(
                  floating: true,
                  pinned: true,
                  elevation: 1,
                  backgroundColor: Colors.white,
                  title: new TextField(
                    onChanged: (text) => search(text),
                    style: new TextStyle(fontSize: 18),
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search',
                      hintStyle: new TextStyle(fontSize: 18),
                    ),
                  ),
                  leading: new Icon(Icons.search),
                  bottom: tabBarItems(),

                )
              ];
            },
            body: tabBarScreens()
        ),
      );
    }

  search(text) async {
      if(index == 0){
        users = [];
        List response = await new SearchClass().user(text);
        print(response);
        if(response == null){
          setState(() {
            users = [];
          });
        }else{

          setState(() {
            users.addAll(response);
          });
        }
      }else{

      }
  }


  void setTabBarIndex() {
      setState(() {
        index = _tabController.index;
      });
  }
}