import 'package:flutter/material.dart';
import  'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';

class ChatPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> {

  SocketIO _socketIO;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
          backgroundColor: Colors.blueGrey,
          automaticallyImplyLeading: false,
          title: new Row(
            children: <Widget>[
              new GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: new Icon(Icons.keyboard_arrow_left,color: Colors.white),
              ),
              new SizedBox(
                width: 8,
              ),
              new Hero(
                tag: 'avatar',
                child: new Container(
                  width: 40,
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://lh3.googleusercontent.com/-K-nqfjer0y4/AAAAAAAAAAI/AAAAAAAAAAA/ACevoQPiF7Jv6q595Hok18uXpu26HqqfjQ/mo/photo.jpg?sz=46'
                          )
                      )
                  ),
                ),
              ),
              new SizedBox(
                width: 8,
              ),
              new Expanded(
                 child: new Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     new Text(
                       'ChatPage',
                       style: TextStyle(fontSize: 16,color: Colors.white),
                     ),
                     new Text(
                         'online',
                       style: TextStyle(fontSize: 14 , color: Colors.green),
                     )
                   ],
                 ),
              ),
              new IconButton(icon: new Icon(Icons.videocam,color: Colors.white,), onPressed: null),
              new IconButton(icon: new Icon(Icons.call,color: Colors.white,), onPressed: null)
            ],
          )
      ),
      body: new Stack(
        children: <Widget>[
          new Align(
            alignment: Alignment.bottomLeft,
            child: new Container(
              decoration: new BoxDecoration(
                gradient: LinearGradient(colors: [new Color(0xAA000000),new Color(0xFF000000)]),
              ),
              child: new Row(
                children: <Widget>[
                  new IconButton(icon: new Icon(Icons.insert_emoticon , color: Colors.white), onPressed: null),
                  new Flexible(
                    child: new TextField(
                      onChanged: (text){print(text);},
                      style: new TextStyle(fontSize: 18,color: Colors.white),
                      decoration: new InputDecoration(
                        hintText: 'type message',
                        hintStyle: new TextStyle(color: Colors.white),
                        border: InputBorder.none,
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  new IconButton(icon: new Icon(Icons.airplanemode_active , color: Colors.white), onPressed: (){print('Send');}),
                ],
              ),
            )
          )
        ],
      ),
      resizeToAvoidBottomPadding: true,
    );
  }
}
