import 'package:flutter/material.dart';
import 'dart:math';

random() {
  var rnd = new Random();
  int min = 1;
  int max = 20;
  var r = min + rnd.nextInt(max - min);
  return '$r';
}

class ChatBarBox extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ChatBarBoxState();

}

class ChatBarBoxState extends State<ChatBarBox> with AutomaticKeepAliveClientMixin<ChatBarBox> {
  final Contacts = new ListView.builder(
    scrollDirection: Axis.horizontal,
    itemCount: 15,
    itemBuilder: (context, index) {
      return new Stack(
        children: <Widget>[
          new Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: 5),
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://lh3.googleusercontent.com/-K-nqfjer0y4/AAAAAAAAAAI/AAAAAAAAAAA/ACevoQPiF7Jv6q595Hok18uXpu26HqqfjQ/mo/photo.jpg?sz=46'
                    )
                )
            ),
          ),
          new Positioned(
            right: 3,
            bottom: 0,
            child: new CircleAvatar(
              radius: 10,
              child: new Text(
                random(),
                style: TextStyle(fontSize: 12),
              ),
            ),
          )
        ],
      );
    },
  );

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return new Material(
      elevation: 2,
      child: new Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            // 10% of the width, so there are ten blinds.
            colors: [const Color(0xFFF44336), const Color(0xFF9C27B0)],
            // whitish to gray whitish to gray
            tileMode: TileMode.repeated, // repeats the gradient over the canvas
          ),
        ),
//      color: new Color(0xffeeeeee),
        height: deviceSize.height * 0.1,
        padding: EdgeInsets.all(8),
        child: Contacts,
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
