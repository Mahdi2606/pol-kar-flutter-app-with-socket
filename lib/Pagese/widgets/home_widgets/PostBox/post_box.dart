import 'package:flutter/material.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_action_bar.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_comment_box.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_header.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_image.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_text.dart';
import 'package:socket_test/model/post_model.dart';





class PostBox extends StatefulWidget {
  final PostModel post;

  const PostBox({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => PostBoxState(); 

}


class PostBoxState extends State<PostBox> with AutomaticKeepAliveClientMixin<PostBox>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  Widget post(){
    return new Column(
      children: <Widget>[
        new PostHeader(post: widget.post,),
        widget.post.photos.length > 0 ? new PostImage(post: widget.post) : new SizedBox(),
        new PostActionBar(post: widget.post),
        widget.post.text.length > 0 ? new PostText(post: widget.post) : new SizedBox(),
        new CommentBox(post: widget.post),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return new Container(
//      margin: EdgeInsets.only(top: 15),
      child: post(),
    );
  }
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}