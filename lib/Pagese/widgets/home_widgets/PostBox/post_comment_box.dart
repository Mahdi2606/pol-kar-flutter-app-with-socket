import 'package:flutter/material.dart';
import 'package:socket_test/model/post_model.dart';


class CommentBox extends StatefulWidget{
  final PostModel post;

  const CommentBox({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => CommentBoxState();
}


class CommentBoxState extends State<CommentBox>{
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(bottom: 15),
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: new TextField(
        decoration: InputDecoration(
          hintText: 'what is in your mind...',
          hintStyle: new TextStyle()
        ),
      ),
    );
  }

}