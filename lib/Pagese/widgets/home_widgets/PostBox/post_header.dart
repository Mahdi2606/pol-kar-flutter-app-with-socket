import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:socket_test/Classes/Navigate.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/ChatPage/chat_page.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:flutter_icons/flutter_icons.dart';


class PostHeader extends StatefulWidget{
  final PostModel post;
  const PostHeader({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => PostHeaderState(); 
}

class PostHeaderState extends State<PostHeader>{


//  final bar =

  @override
  Widget build(BuildContext context) {
    return new Container(
      // color: new Color(),
      child: new Padding(
        padding: EdgeInsets.all(5),
        child: new GestureDetector(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Container(
                    width: 40,
                    height: 40,
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                'https://lh3.googleusercontent.com/-K-nqfjer0y4/AAAAAAAAAAI/AAAAAAAAAAA/ACevoQPiF7Jv6q595Hok18uXpu26HqqfjQ/mo/photo.jpg?sz=46'
                            )
                        )
                    ),
                  ),
                  new Text(widget.post.username),
                ],
              ),
              new Padding(

                padding: EdgeInsets.only(right: 12),
                child: new GestureDetector(
                    child: new Icon(Ionicons.getIconData('ios-quote') , size: 14,),
                    onTap: (){
                      Navigator.push(
                          context,
                          new CupertinoPageRoute(builder: (context) => ChatPage())
                      );
                    }
                ),
              )
            ],
          ),
          onTap: (){
            Navigate.navigateToUserPage(
                context,
                {'username' : widget.post.username},
                transition: 'horizantal'
            );
          },
        )
      ),
    );
  }

}
