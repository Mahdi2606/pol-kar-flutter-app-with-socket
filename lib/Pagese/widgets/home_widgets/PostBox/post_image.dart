import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:percent_indicator/percent_indicator.dart';
class PostImage extends StatefulWidget {
  final PostModel post;

  const PostImage({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => PostImageState();
}


class PostImageState extends State<PostImage>{
PageController _pageController;

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: 0 , );
  }


  @override
  Widget build(BuildContext context) {
    return new Material(
      elevation: 1,
      child: new AspectRatio(
        aspectRatio: 1.2/1,
        child: new Container(
          width: MediaQuery.of(context).size.width,
          height: 400,
          child: new PageView.builder(
            controller: _pageController,
            scrollDirection: Axis.horizontal,
            itemCount: widget.post.photos.length,
            itemBuilder: (context , index){
              return new Container(
                  width: MediaQuery.of(context).size.width,
                  child: TransitionToImage(
                    image: AdvancedNetworkImage('http://192.168.43.3:2000/files/${widget.post.photos[index]}'),
                    loadingWidgetBuilder: (double progress) =>  CircularPercentIndicator(
                      radius: 40.0,
                      lineWidth: 1.0,
                      center: new Center(child: new Text('${(progress * 100).toInt()}' , style: TextStyle(fontSize: 9),)),
                      animation: true,
                      percent: progress,
                      progressColor: Colors.green,
                      animateFromLastPercent: true,
                    ),
                    placeholder: const Icon(Icons.refresh),
                    width: 400.0,
                    height: 300.0,
                    enableRefresh: true,
                  )
              );
            },
          ),
        ),
      )
    );
  }
}