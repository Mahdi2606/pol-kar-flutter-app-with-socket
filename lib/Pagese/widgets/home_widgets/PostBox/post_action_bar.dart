import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/model/post_model.dart';

class PostActionBar extends StatefulWidget {
  final PostModel post;
  const PostActionBar({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => PostActionBarState();
}

class PostActionBarState extends State<PostActionBar> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      // color: Colors.redAccent,
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Container(
              child: new Row(
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: new GestureDetector(
                      child: Icon(
                        Ionicons.getIconData('ios-heart-empty'),
                        color: Colors.black,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  new Text(
                    widget.post.like.toString(),
                    style: TextStyle(fontSize: 12),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  new Icon(Ionicons.getIconData('ios-chatbubbles')),
                  SizedBox(
                    width: 10,
                  ),
                  new Text(
                    widget.post.commentCount.toString(),
                    style: TextStyle(fontSize: 12),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  new Icon(Ionicons.getIconData('ios-eye')),
                  SizedBox(
                    width: 10,
                  ),
                  new Text(
                    widget.post.view.toString(),
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
          ),
          new IconButton(icon: new Icon(Ionicons.getIconData('md-share-alt')), onPressed: (){}),
        ],
      ),
    );
  }
}
