import 'package:flutter/material.dart';
import 'package:socket_test/model/post_model.dart';


class PostText extends StatefulWidget{
  final PostModel post;

  const PostText({Key key,@required this.post}) : super(key: key);
  @override
  State<StatefulWidget> createState() => PostTextState();
}


class PostTextState extends State<PostText>{
  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.all(10),
          child: new Align(
            alignment: Alignment.centerLeft,
            child: new Container(
                child : new Text(widget.post.text.toString())
            ),
          ),
        )
      ],
    );
  }
}

