import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/Classes/post_controller.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/Pagese/profile_edit_page.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_box.dart';
import 'package:socket_test/Widgets/posts_widget.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:socket_test/model/user_model.dart';



class UserPage extends StatefulWidget {
  final username;
  const UserPage({Key key, @required this.username}) : super(key: key);
  @override
  State<StatefulWidget> createState() => UserPageState();
}

class UserPageState extends State<UserPage> with SingleTickerProviderStateMixin {
  bool _spinner = true;
  int _follow;
  UserModel _user;
  int _currentPage;
  List<PostModel> _posts = [];
  ScrollController _scrollController = new ScrollController();
  TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initData();
    _tabController = new TabController(length: 3, vsync: this);
    _scrollController.addListener((){
      double _maxScroll = _scrollController.position.maxScrollExtent;
      double _currentScroll = _scrollController.position.pixels;
      if(_currentScroll > _maxScroll - 1){
        _givePosts(page: _currentPage + 1);
      }
    });
  }
  _initData() async {
    await _giveUser();
    await _givePosts();
  }
  Future _giveUser() async {
    Map user = await new UserController().giveUser(widget.username);
    setState(() {
      _user = user['user'];
      _follow = _user.followMode;
      _spinner = false;
    });
  }
  Future _givePosts({ int page : 1 , bool refresh = false}) async {
    if(refresh) _posts.clear();
    Map posts = await new PostController().getUserPosts(widget.username , page );
    setState(() {
      _posts.addAll(posts['posts']);
      _currentPage = posts['currentPage'];
    });
  }


  Widget appBar(){
    return new SliverAppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      snap: true,
      expandedHeight: 130,
      elevation: 1,
      flexibleSpace: FlexibleSpaceBar(
        background: new Container(
          decoration: new BoxDecoration(
            color: new Color(0x77E3F2FD),
          ),
          child: new Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: new Stack(
              children: <Widget>[
                new Align(
                    alignment: Alignment.centerLeft,
                    child: new Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new GestureDetector(
                              onVerticalDragUpdate: (DragUpdateDetails details) {
                                print(details);
                              },
                              child: new Container(
                                height: 100,
                                width: 100,
                                decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(50)),
                                    color: Colors.cyan
                                ),
                              ),
                              onTap: (){
                                //nothing
                              },
                            ),
                            new Padding(
                              padding: EdgeInsets.only(top: 5),
                              child: new Text(_user.username.toString()),
                            )
                          ],
                        ),
                        new Expanded(
                          child: new Stack(
                            children: <Widget>[
                              new Align(
                                alignment: Alignment.centerRight,
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                        child: new Padding(
                                          padding: EdgeInsets.only(top: 20),
                                          child: new Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              new Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  new Text('Follower' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                  new SizedBox(height: 8,),
                                                  new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                ],
                                              ),
                                              new Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  new Text('Following' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                  new SizedBox(height: 8,),
                                                  new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                    new GestureDetector(
                                      onTap: _handleFollow,
                                      child: new Container(
                                          width: 150,
                                          height: 30,
                                          decoration: new BoxDecoration(
                                              color: (_follow == 1)
                                                  ? Colors.red[200]
                                                  : (_follow == 0)
                                                  ? Colors.cyan[200]
                                                  : Colors.grey[300],
                                              borderRadius: BorderRadius.all(Radius.circular(20))
                                          ),
                                          margin: EdgeInsets.only(top: 10),
                                          child: new Center(
                                            child: _follow == 1
                                                ? new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.directions_walk, size: 15,) , SizedBox(width: 5,) , new Text('unFollow')],)
                                                : _follow == 0
                                                ? new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.directions_run , size: 15,) , SizedBox(width: 5,) , new Text('Follow')],)
                                                : new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.alarm , size: 15,) , SizedBox(width: 5,) , new Text('requested')],),
                                          )
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                ),
              ],
            ),
          ),
        ),
      ),
      floating: true,
    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      backgroundColor: Colors.white,
      body: _spinner
          ? new Center(child: CupertinoActivityIndicator(),)
          : new NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
          return [
            appBar()
          ];
        },
        body: new Stack(
          children: <Widget>[
            new TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                PostWidget.postList(_posts, _onRefresh),
                PostWidget.gridPost(_posts, _onRefresh),
                new Container(color: Colors.white,child: new Center(child: new Text('Soon..!'),),),
              ],
              controller: _tabController,
            ),
            new Align(
                alignment: Alignment.bottomCenter,
                child: new BackdropFilter(
                  filter: ImageFilter.blur(sigmaY: 5 , sigmaX: 5),
                  child: new Container(
                    decoration: new BoxDecoration(
                        color: Colors.white70
                    ),
                    child: new TabBar(
                        indicatorColor: Colors.transparent,
                        labelColor: Colors.black,
                        controller: _tabController,

                        tabs: <Widget>[
                          new Tab(icon: Icon(Ionicons.getIconData('ios-list')),),
                          new Tab(icon: Icon(Ionicons.getIconData('ios-grid')),),
                          new Tab(icon: Icon(Ionicons.getIconData('md-pricetags')),),
                        ]
                    ),
                  ),
                )
            )
          ],
        ),
      )
    );
  }

  _handleFollow() async {
    if(_follow == 1){
      Map response = await new UserController().unFollowUser(widget.username);
      if(response['status']){
        GlobalClass.showFlushBar(context, response['message']);
        setState(() {
          _follow = 0;
        });
      }else{
        GlobalClass.showFlushBar(context, response['message']);
      }

    }else if(_follow == 0){
      Map response = await new UserController().followUser(widget.username);
      if(response['status']){
        GlobalClass.showFlushBar(context, response['message']);
        setState(() {
          if(_user.settings['pageMode']){
            setState(() {
              _follow = 2;
            });
          }else{
            setState(() {
              _follow = 1;
            });
          }
        });
      }else{
        GlobalClass.showFlushBar(context, response['message']);
      }
    }
  }
  Future<Null> _onRefresh() async {
    await _givePosts(refresh: true);
    return null;
  }
}
