import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_test/Classes/post_controller.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/Pagese/profile_edit_page.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/PostBox/post_box.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:socket_test/model/user_model.dart';



class UserPage extends StatefulWidget {
  final username;
  const UserPage({Key key, @required this.username}) : super(key: key);
  @override
  State<StatefulWidget> createState() => UserPageState();
}

class UserPageState extends State<UserPage> {
  bool _spinner = true;
  int _follow;
  UserModel _user;
  int _currentPage;
  List<PostModel> _posts = [];
  ScrollController _scrollController = new ScrollController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initData();
    _scrollController.addListener((){
      double _maxScroll = _scrollController.position.maxScrollExtent;
      double _currentScroll = _scrollController.position.pixels;
      if(_currentScroll > _maxScroll - 1){
        _givePosts(page: _currentPage + 1);
      }
    });
  }
  _initData() async {
    await _giveUser();
    await _givePosts();
  }

  Future _giveUser() async {
    Map user = await new UserController().giveUser(widget.username);
    setState(() {
      _user = user['user'];
      _follow = _user.followMode;
      _spinner = false;
    });
  }
  Future _givePosts({ int page : 1 , bool refresh = false}) async {
    if(refresh) _posts.clear();
    Map posts = await new PostController().getUserPosts(widget.username , page );
    setState(() {
      _posts.addAll(posts['posts']);
      _currentPage = posts['currentPage'];
    });
  }
  @override
  Widget build(BuildContext context) {
    return showPage();
  }


  showPage(){
    if(_spinner){
      return new Scaffold(
        body: new Center(
          child: new CupertinoActivityIndicator(),
        ),
      );
    }else{
      return new CupertinoTabScaffold(
          tabBuilder: (context , index){
            return userPageScaffold(index);
          },
          tabBar: new CupertinoTabBar(
            backgroundColor: Color(0x55FFFFFF),
            iconSize: 23,
            inactiveColor: Colors.black,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.list)),
              BottomNavigationBarItem(icon: Icon(Icons.grid_on)),
              BottomNavigationBarItem(icon: Icon(Icons.attachment)),
              BottomNavigationBarItem(icon: Icon(Icons.bookmark_border))
            ],
          )
      );
    }
  }

  //page-scaffold
  Widget userPageScaffold(index){
    return new Scaffold(
      body: new RefreshIndicator(
          child: new CustomScrollView(
            controller: _scrollController,
            slivers: <Widget>[
              new SliverAppBar(
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                snap: true,
                expandedHeight: 130,
                elevation: 1,
                flexibleSpace: FlexibleSpaceBar(
                  background: new Container(
                    decoration: new BoxDecoration(
                      color: new Color(0x77E3F2FD),
                    ),
                    child: new Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: new Stack(
                        children: <Widget>[
                          new Align(
                              alignment: Alignment.centerLeft,
                              child: new Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new GestureDetector(
                                        onVerticalDragUpdate: (DragUpdateDetails details) {
                                          print(details);
                                        },
                                        child: new Container(
                                          height: 100,
                                          width: 100,
                                          decoration: new BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(50)),
                                              color: Colors.cyan
                                          ),
                                        ),
                                        onTap: (){
                                          //nothing
                                        },
                                      ),
                                      new Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: new Text(_user.username.toString()),
                                      )
                                    ],
                                  ),
                                  new Expanded(
                                    child: new Stack(
                                      children: <Widget>[
                                        new Align(
                                          alignment: Alignment.centerRight,
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Container(
                                                  child: new Padding(
                                                    padding: EdgeInsets.only(top: 20),
                                                    child: new Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      children: <Widget>[
                                                        new Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            new Text('Follower' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                            new SizedBox(height: 8,),
                                                            new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                          ],
                                                        ),
                                                        new Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            new Text('Following' , style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                                            new SizedBox(height: 8,),
                                                            new Text('2000',style: new TextStyle(color: Colors.black54),)
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                              ),
                                              new GestureDetector(
                                                onTap: (){
                                                  setState(() {
                                                    if(_follow == 1){
                                                      _follow = 0;
                                                    }else if(_follow == 0){
                                                      if(_user.settings['pageMode']){
                                                        _follow = 2;
                                                      }else{
                                                        _follow = 1;
                                                      }
                                                    }
                                                  });
                                                },
                                                child: new Container(
                                                    width: 150,
                                                    height: 30,
                                                    decoration: new BoxDecoration(
                                                        color: (_follow == 1)
                                                            ? Colors.red[200]
                                                            : (_follow == 0)
                                                            ? Colors.cyan[200]
                                                            : Colors.grey[300],
                                                        borderRadius: BorderRadius.all(Radius.circular(20))
                                                    ),
                                                    margin: EdgeInsets.only(top: 10),
                                                    child: new Center(
                                                      child: _follow == 1
                                                          ? new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.directions_walk, size: 15,) , SizedBox(width: 5,) , new Text('unFollow')],)
                                                          : _follow == 0
                                                          ? new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.directions_run , size: 15,) , SizedBox(width: 5,) , new Text('Follow')],)
                                                          : new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[new Icon(Icons.alarm , size: 15,) , SizedBox(width: 5,) , new Text('requested')],),
                                                    )
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                floating: true,
              ),
              new SliverList(
                delegate: new SliverChildListDelegate([
                  pages(index),
                ]),
              ),

            ],
          ),
          onRefresh: _handleRefresh
      ),
      backgroundColor: Colors.white,
    );
  }

  //page-app-bar
//  Widget appBar(){
//    return
//  }

  //tab-bar-items
//  Widget cupertinoTabBar(){
//    return
//  }

  //tab-bar-pages-view
  pages(index) {
    switch(index){
      case 0 : {
        return new Center(
          child:  _posts.length == 0
              ? new Center(child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(Icons.filter_none,size: 25,color: Colors.grey,) ,
              new SizedBox(width: 10,) ,
              new Text('No post', style: new TextStyle(color: Colors.grey),)
            ],
          ),)
              : new ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: _posts.length,
            itemBuilder: (context , index){
              return PostBox(post: _posts[index],);
            },
          ),
        );
      }
      break;
      case 1 : {
        return new RefreshIndicator(
            child: new Center(
                child: _posts.length == 0
                    ? new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Icon(Icons.filter_none,size: 25,color: Colors.grey,) ,
                    new SizedBox(width: 10,) ,
                    new Text('No post', style: new TextStyle(color: Colors.grey),)
                  ],
                )
                    : new GridView.builder(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  padding: EdgeInsets.only(top: 0),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  itemCount: _posts.length,
                  itemBuilder: (context , index){
                    return new Padding(
                      padding: EdgeInsets.all(5),
                      child: new Container(
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: _posts[index].photos.length <= 0 ? new AssetImage('assets/images/no_image.png',) : new NetworkImage(
                                'http://192.168.43.3:2000/files/${_posts[index].photos[0]}',
                              ),
                              fit: BoxFit.cover,
                            ),
                            boxShadow: <BoxShadow>[BoxShadow(color: Colors.grey , offset: Offset(0, 0) , blurRadius: 1)],
                          ),
                          child: new Stack(
                            children: <Widget>[
                              new Opacity(
                                opacity: .6,
                                child: new Container(
                                  color: Colors.black,
                                ),
                              ),
                              new Align(
                                alignment: Alignment.center,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Icon(Ionicons.getIconData('ios-heart') , color: Colors.white,),
                                        new SizedBox(height: 5,),
                                        new Text('${_posts[index].like}' , style: new TextStyle(color: Colors.white),),
                                      ],
                                    ),
                                    new SizedBox(width: 10,),
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Icon(Ionicons.getIconData('ios-eye') , color: Colors.white,),
                                        new SizedBox(height: 5,),
                                        new Text('${_posts[index].view}' , style: new TextStyle(color: Colors.white),),
                                      ],
                                    ),
                                    new SizedBox(width: 10,),
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Icon(Ionicons.getIconData('ios-chatbubbles') , color: Colors.white,),
                                        new SizedBox(height: 5,),
                                        new Text('${_posts[index].commentCount}' , style: new TextStyle(color: Colors.white),),
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          )
                      ),
                    );
                  },
                )
            ),
            onRefresh: _handleRefresh
        );
      }
      break;
      case 2 : {
        return new Center(
          child: new Text('pin'),
        );
      }
      break;
      case 3 : {
        return new Center(
          child: new Text('bookmark'),
        );
      }
      break;
      default : {
        return new Center(
          child: new Text('ERROR'),
        );
      }
    }
  }

  //post-list-view
//  Widget listView(){
//    return
//}

  //post-grid-view
//  Widget gridView(){
//    return
//  }


  Future<Null> _handleRefresh() async {
    await _givePosts(refresh: true);
    return null;
  }
}
