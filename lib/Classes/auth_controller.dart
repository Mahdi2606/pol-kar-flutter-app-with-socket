import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:socket_test/Classes/Navigate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_test/model/user_model.dart';

class AuthController {
  final String globalUrl = 'http://192.168.43.3:2000/v1';


  Future<Map> login(Map body) async {
    var dio = new Dio();
    final response = await dio.post('$globalUrl/login' , data : body);
    var responseData = response.data;
    return responseData;
  }
  

  Future<Map> register(Map body) async {
    final response = await http.post('$globalUrl/register' , body: body);
    var responseData = json.decode(response.body);
    return responseData;
  }

  Future<Map> checkApi(String token) async {
    var dio = new Dio();
    final response = await dio.get('$globalUrl/user?token=$token');
    Map data = response.data['data'];
    UserModel currentUser = UserModel.fromJson(data);
    return {
      "currentUser" : currentUser,
    };
  }


  static setUserData(@required BuildContext context , Map data) async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    perfs.setString('user_api_token', data['token']);
    Navigate.navigateToSplashScreen(context);
  }
}