import 'package:dio/dio.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:socket_test/model/post_model.dart';

class PostController extends GlobalClass {

  Future getUserPosts(username,page) async {
    var dio = new Dio();
    String token = await UserController.getToken();
    Response response = await dio.get('$globalUrl/posts/$username?token=$token&page=$page');
    List postsData = response.data['data'];
    int currentPage = response.data['page'];
    List<PostModel> posts = [];
    postsData.forEach((item){
      posts.add(PostModel.fromJson(item));
    });

    return {
      'posts' : posts.reversed,
      'currentPage' : currentPage,
    };

  }
  Future getFollowedUsersPosts(page) async {
    var dio = new Dio();
    String token = await UserController.getToken();
    print(token);
    Response response = await dio.get('$globalUrl/getposts?token=$token&page=${page.toString()}');
    List postsData = response.data['data'];
    int currentPage = response.data['page'];
    List<PostModel> posts = [];
    postsData.forEach((item){
      posts.add(PostModel.fromJson(item));
    });

    return {
      'posts' : posts.reversed,
      'currentPage' : currentPage,
    };
  }

}