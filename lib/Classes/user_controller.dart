import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/model/post_model.dart';
import 'package:socket_test/model/user_model.dart';

class UserController extends GlobalClass{

  static Future<String> getToken() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    String token = perfs.getString('user_api_token');
    return token;
  }

  Future<Map> giveUser(String username) async {
    String token = await getToken();
    var dio = new Dio();
    Response response = await dio.get('${this.globalUrl}/user/$username?token=$token');
    var result = response.data['data'];
    UserModel user = UserModel.fromJson(result);
    return {
      "user" : user,
    };
  }

  Future getPost(id) async {
    String token = await getToken();
    var dio = new Dio();
    Response res = await dio.get('$globalUrl/post/$id?token=$token');
    if(res.data['status']){
      return res.data['data']['post'];
    }else{
      return false;
    }
  }

  Future updateUser(Map _data) async {
    var dio = new Dio();
    String token = await getToken();
    Response response = await dio.post('$globalUrl/user?token=$token' , data: _data);
    Map data = response.data['data'];
    var status = response.data['status'];
    if(status){
      UserModel currentUser = UserModel.fromJson(data);
      return {
        "currentUser" : currentUser,
        "status" : true
      };
    }else{
      return {
        "currentUser" : null,
        "status" : false
      };
    }

  }

  Future followUser(username) async {
    var dio = new Dio();
    String token = await getToken();
    Response response = await dio.get('$globalUrl/follow/$username?token=$token');
    Map responseData = response.data;
    return {
      'status' : responseData['status'],
      'message' : responseData['message']
    };
  }

  Future unFollowUser(username) async {
    var dio = new Dio();
    String token = await getToken();
    Response response = await dio.delete('$globalUrl/follow/$username?token=$token');
    Map responseData = response.data;
    return {
      'status' : responseData['status'],
      'message' : responseData['message']
    };
  }

}