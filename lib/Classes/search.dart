

import 'package:dio/dio.dart';
import 'package:socket_test/Classes/auth_controller.dart';
import 'package:socket_test/Classes/global.dart';
import 'package:socket_test/Classes/user_controller.dart';

class SearchClass extends GlobalClass {

  Future user(text) async {
    var dio = Dio();
    String token = await UserController.getToken();
    Response response = await dio.get(
        '${this.globalUrl}/user/search/$text?token=$token' ,
    );
    return response.data['data'];
  }

//  Future post(text) async {
//    var dio = Dio();
//    String token = await UserController.getToken();
//    Response response = await dio.get(
//      '${this.globalUrl}/user/search/$text?token=$token' ,
//    );
//    return response.data['data'];
//  }


}