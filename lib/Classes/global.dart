import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:socket_test/Classes/user_controller.dart';
import 'package:flushbar/flushbar.dart';

class GlobalClass {
  final globalUrl = 'http://192.168.43.3:2000/v1';

  static Future<bool> checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi;
  }

  static showSnackBar(GlobalKey<ScaffoldState> pageKey , Widget content , {int time : 1000 , Color backgroundColor : Colors.black54}){
    pageKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: backgroundColor,
        duration: Duration(milliseconds: time),
        content: content,
      )
    );

  }

  static showFlushBar(context,message){
    Flushbar(
      animationDuration: Duration(milliseconds: 700),
      aroundPadding: EdgeInsets.all(8),
      borderRadius: 8,
      message: message,
      duration:  Duration(milliseconds: 2000),
      forwardAnimationCurve: Curves.easeOutExpo,
    )..show(context);
  }
}