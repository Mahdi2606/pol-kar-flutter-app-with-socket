import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' show Navigator;
import 'package:flutter/material.dart' show BuildContext;
import 'package:flutter/material.dart' show MaterialPageRoute;
import 'package:socket_test/Pagese/my_home_page.dart';
import 'package:socket_test/Pagese/profile_edit_page.dart';
import 'package:socket_test/Pagese/widgets/user_widgets/user_page.dart';

class Navigate {

  static navigateToHomeScreen(BuildContext context){
    Navigator.pushReplacement(
        context,
        new MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }

  static navigateToSplashScreen(BuildContext context){
    Navigator.pushReplacementNamed(context, '/');
  }

  static navigateToUserPage(BuildContext context , Map data , {transition = 'vertical'}){
    if(transition == 'vertical'){
      Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context)=>new UserPage(username: data['username'],)));
    }else if(transition == 'horizantal'){
      Navigator.push(context, new CupertinoPageRoute(builder: (BuildContext context) => new UserPage(username: data['username'],)));
    }
  }

  static navigateToProfileEditPage(BuildContext context , {transition = 'vertical'}){
    if(transition == 'vertical'){
      Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context)=>new ProfileEditPage()));
    }else if(transition == 'horizantal'){
      Navigator.push(context, new CupertinoPageRoute(builder: (BuildContext context) => new ProfileEditPage()));
    }
  }
}