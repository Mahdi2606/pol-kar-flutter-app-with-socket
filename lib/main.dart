import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:socket_test/Pagese/auth_page.dart';
import 'package:socket_test/Pagese/login_page.dart';
import 'package:socket_test/Pagese/my_home_page.dart';
import 'package:socket_test/Pagese/register_page.dart';
import 'package:socket_test/Pagese/splash_screen.dart';
import 'package:socket_test/Pagese/widgets/home_widgets/ChatPage/chat_page.dart';
import 'package:socket_test/Test/test.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:socket_test/scoped_model/AppModel.dart';


void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(
        new ScopedModel<AppModel>(
            model: AppModel(),
            child: MyApp()
        )
    );
  });
}





class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      color: Colors.red,
      theme: new ThemeData(
        accentColor: Colors.black,
        primaryIconTheme: IconThemeData(color: Colors.black),
        primaryTextTheme: TextTheme(title: TextStyle(color: Colors.black)),
      ),
      debugShowCheckedModeBanner: false,
      title: 'پول کار',
      routes: {
        "/" : (context) => new SplashScreen(),
//        "/" : (context) => new TestPage(),
//        "/" : (context) => new MyHomePage(),
        "/auth" : (context) => new AuthPage()
      },
    );
  }



}

